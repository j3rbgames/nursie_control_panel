import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from './components/Dashboard.vue';

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/',
			name: 'dashboard',
			component: Dashboard
		},
		{
			path: '/page1',
			name: 'page1',
			component: () => import('./components/Page1.vue')
		},
		{
			path: '/page2',
			name: 'page2',
			component: () => import('./components/Page2.vue')
		},
		{
			path: '/page3',
			name: 'page3',
			component: () => import('./components/Page3.vue')
		},
		{
			path: '/control_remoto',
			name: 'control_remoto',
			component: () => import('./components/PageControlRemoto.vue')
		},
	],
	scrollBehavior() {
		return {x: 0, y: 0};
	}
});